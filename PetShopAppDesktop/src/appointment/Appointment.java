/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment;

import database.AppointmentVetDB;

import java.util.Date;
import java.util.List;

/**
 * @author Daniel Salgado
 */
public class Appointment {

    private int idAppointment;
    private String petName;
    private String petSpecie;
    private String petBreed;
    private String petResponsibleEmail;
    private String petResponsibleName;
    private Date dtAppointment;
    private String petTypeAppointment;
    private String petResponsiblePhone;

    /**
     * @return the petName
     */
    public String getPetName() {
        return petName;
    }

    /**
     * @param petName the petName to set
     */
    public void setPetName(String petName) {
        this.petName = petName;
    }

    /**
     * @return the petSpecie
     */
    public String getPetSpecie() {
        return petSpecie;
    }

    /**
     * @param petSpecie the petSpecie to set
     */
    public void setPetSpecie(String petSpecie) {
        this.petSpecie = petSpecie;
    }

    /**
     * @return the petBreed
     */
    public String getPetBreed() {
        return petBreed;
    }

    /**
     * @param petBreed the petBreed to set
     */
    public void setPetBreed(String petBreed) {
        this.petBreed = petBreed;
    }

    /**
     * @return the petResponsibleEmail
     */
    public String getPetResponsibleEmail() {
        return petResponsibleEmail;
    }

    /**
     * @param petResponsibleEmail the petResponsibleEmail to set
     */
    public void setPetResponsibleEmail(String petResponsibleEmail) {
        this.petResponsibleEmail = petResponsibleEmail;
    }

    /**
     * @return the petResponsibleName
     */
    public String getPetResponsibleName() {
        return petResponsibleName;
    }

    /**
     * @param petResponsibleName the petResponsibleName to set
     */
    public void setPetResponsibleName(String petResponsibleName) {
        this.petResponsibleName = petResponsibleName;
    }

    /**
     * @return the dtAppointment
     */
    public Date getDtAppointment() {
        return dtAppointment;
    }

    /**
     * @param dtAppointment the dtAppointment to set
     */
    public void setDtAppointment(Date dtAppointment) {
        this.dtAppointment = dtAppointment;
    }

    /**
     * This method will call AppointmentVetDB.save() in order to store the class into database
     */
    public void save() {
        AppointmentVetDB newAppointment = new AppointmentVetDB(this);
        newAppointment.save();
    }

    /**
     *
     * @return List of strings of availables times
     * @throws Exception Throws execption in case of error
     */
    public List<String> getAvailabeTimes() throws Exception {

        AppointmentVetDB ap = new AppointmentVetDB(this);

        List<String> times = null;

        try {
            times = ap.getAvailableTimes();
        } catch (Exception e) {
            System.out.println("Error getting availabe times: " + e.getMessage());
            throw e;
        } finally {
            return times;
        }

    }

    /**
     * This method will return a list of Appointments which is already stored into database
     * @return list of appointments
     */
    public List<Appointment> getAppointments()
    {
        
        AppointmentVetDB getAppointments = new AppointmentVetDB(null);
        
        return getAppointments.getAppointments();
        
    }

    /**
     * @return the petTypeAppointment
     */
    public String getPetTypeAppointment() {
        return petTypeAppointment;
    }

    /**
     * @param petTypeAppointment the petTypeAppointment to set
     */
    public void setPetTypeAppointment(String petTypeAppointment) {
        this.petTypeAppointment = petTypeAppointment;
    }

    /**
     * @return the petResponsiblePhone
     */
    public String getPetResponsiblePhone() {
        return petResponsiblePhone;
    }

    /**
     * @param petResponsiblePhone the petResponsiblePhone to set
     */
    public void setPetResponsiblePhone(String petResponsiblePhone) {
        this.petResponsiblePhone = petResponsiblePhone;
    }

    /**
     * @return the idAppointment
     */
    public int getIdAppointment() {
        return idAppointment;
    }

    /**
     * @param idAppointment the idAppointment to set
     */
    public void setIdAppointment(int idAppointment) {
        this.idAppointment = idAppointment;
    }

    /**
     * This method will delete from the database appointments
     * @return true or false
     */
    public boolean cancelApointment()
    {
        AppointmentVetDB newAppointment = new AppointmentVetDB(this);
        newAppointment.cancelAppointment();
        
        return true;
    }
    
}
