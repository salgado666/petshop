/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import java.time.LocalDateTime;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

/**
 * FXML Controller class
 *
 * @author danie
 */
public class AppointmentListController implements Initializable {

    @FXML
    private TableView<Appointment> tbvAppointments;

    @FXML
    private Button btnCancel;

    /**
     * Initializes the controller class.
     *
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        getAppointments();

        // TODO
    }

    private void getAppointments() {

        Appointment ap = new Appointment();

        ObservableList<Appointment> data = FXCollections.observableArrayList(ap.getAppointments());

        TableColumn petName = new TableColumn("Pet Name");
        petName.setMinWidth(100);
        petName.setCellValueFactory(new PropertyValueFactory<>("petName"));

        TableColumn petSpecie = new TableColumn("Specie");
        petSpecie.setMinWidth(50);
        petSpecie.setCellValueFactory(new PropertyValueFactory<>("petSpecie"));

        TableColumn petBreed = new TableColumn("Breed");
        petBreed.setMinWidth(50);
        petBreed.setCellValueFactory(new PropertyValueFactory<>("petBreed"));

        TableColumn petResponsibleName = new TableColumn("Pet Responsible Name");
        petResponsibleName.setMinWidth(200);
        petResponsibleName.setCellValueFactory(new PropertyValueFactory<>("petResponsibleName"));

        TableColumn petResponsiblePhone = new TableColumn("Phone");
        petResponsiblePhone.setMinWidth(100);
        petResponsiblePhone.setCellValueFactory(new PropertyValueFactory<>("petResponsiblePhone"));

        TableColumn petResponsibleEmail = new TableColumn("Email");
        petResponsibleEmail.setMinWidth(200);
        petResponsibleEmail.setCellValueFactory(new PropertyValueFactory<>("petResponsibleEmail"));

        TableColumn petTypeApointment = new TableColumn("Type");
        petTypeApointment.setMinWidth(50);
        petTypeApointment.setCellValueFactory(new PropertyValueFactory<>("petTypeAppointment"));

        TableColumn<Appointment, LocalDateTime> dtApointment = new TableColumn("Date");
        dtApointment.setMinWidth(200);
        dtApointment.setCellValueFactory(new PropertyValueFactory<Appointment, LocalDateTime>("dtAppointment"));

        tbvAppointments.getColumns().clear();
        tbvAppointments.setItems(data);
        tbvAppointments.getColumns().addAll(petName, petSpecie, petBreed, petResponsibleName, petResponsiblePhone, petResponsibleEmail, petTypeApointment, dtApointment);

    }

    @FXML
    void btnCancelApointmentAction(ActionEvent event) {

        Appointment appointmentToCancel = new Appointment();

        appointmentToCancel = tbvAppointments.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirm cancelation");
        alert.setHeaderText("Cancel appointment with " + appointmentToCancel.getPetName() + " the " + appointmentToCancel.getPetSpecie() + "?");
        alert.setContentText("Are you ok with this?");

        Optional<ButtonType> result = alert.showAndWait();
       
        if (result.get() == ButtonType.OK) {
            System.out.println("Will delete: " + appointmentToCancel.getIdAppointment());

            if (appointmentToCancel.cancelApointment() == true) {
                alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Done");
                alert.setHeaderText("Done");
                alert.setContentText("Appointment with " + appointmentToCancel.getPetName() + " the " + appointmentToCancel.getPetSpecie() + " has been canceled");
                alert.showAndWait();
            }

            getAppointments();
            
        }

    }

}
