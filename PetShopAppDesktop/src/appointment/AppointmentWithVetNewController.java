/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment;

import java.net.URL;

import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.util.List;
import java.io.PrintWriter;
import java.io.StringWriter;

import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Daniel Salgado
 */
public class AppointmentWithVetNewController implements Initializable {

    @FXML
    private TextField txtPetResponsible;
    @FXML
    private TextField txtPetResponsiblePhone;
    @FXML
    private TextField txtPetResponsibleEmail;
    @FXML
    private TextField txtPetName;
    @FXML
    private TextField txtPetBreed;
    @FXML
    private ToggleButton tglDog;
    @FXML
    private ToggleButton tglCat;
    @FXML
    private Button btnAppointmentBook;
    @FXML
    private DatePicker dtAppointment;
    @FXML
    private ComboBox<String> cboAppointmentTime;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    /**
     * This button will tag the pet as Cat
     * @param event
     */
    @FXML
    void btnCatAction(ActionEvent event) {
        tglDog.setSelected(false);
        tglCat.setSelected(true);

    }

    /**
     * This button will tag the pet as Dog
     * @param event
     */
    @FXML
    void btnDogAction(ActionEvent event) {
        tglDog.setSelected(true);
        tglCat.setSelected(false);
    }

    /**
     * Tis button will make make the appointment by calling DTO class Appointment.java
     * @param event
     */
    @FXML
    void bntApointmentBookAction(ActionEvent event) {

        Appointment newAppointment = new Appointment();

        newAppointment.setPetResponsibleName(txtPetResponsible.getText());
        newAppointment.setPetResponsiblePhone(txtPetResponsiblePhone.getText());
        newAppointment.setPetResponsibleEmail(txtPetResponsibleEmail.getText());
        newAppointment.setPetName(txtPetName.getText());
        newAppointment.setPetBreed(txtPetBreed.getText());



        try {
            newAppointment.setDtAppointment(java.sql.Date.valueOf(dtAppointment.getValue()));

            String aux = newAppointment.getDtAppointment() + " " + cboAppointmentTime.getValue();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            System.out.println(formatter.parse(aux));
            newAppointment.setDtAppointment(formatter.parse(aux));
        } catch (Exception e) {
            System.out.println("Error parsing string to time");
        }

        if (tglCat.isSelected()) {
            newAppointment.setPetSpecie("Cat");
        } else {
            newAppointment.setPetSpecie("Dog");
        }

        newAppointment.setPetTypeAppointment("With Vet");

        newAppointment.save();

        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Nice =]");
        alert.setAlertType(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Appointment ok");
        alert.setContentText("Soon you will receive an email of confirmation");
        alert.setTitle(null);

        alert.showAndWait();

        Stage stage = (Stage) btnAppointmentBook.getScene().getWindow();
        stage.hide();

    }

    /**
     * DatePicker change event.
     * On changing value to any other date then the previous one, this will call the method getAvailableTimes() to get availables times for that date
     * @param event
     */
    @FXML
    void dtpChangedValue(ActionEvent event) {
        System.out.println("Value has changed to " + dtAppointment.getValue());

        getAvailableTimes();

    }

    /**
     * Method to return availables times from a date.
     * Alert code taken from http://code.makery.ch/blog/javafx-dialogs-official/
     */
    private void getAvailableTimes() {

        Appointment chkVa = new Appointment();
        chkVa.setDtAppointment(java.sql.Date.valueOf(dtAppointment.getValue()));

        try {

            List<String> nS = chkVa.getAvailabeTimes();
            cboAppointmentTime.getItems().clear();
            cboAppointmentTime.getItems().addAll(nS);
        } catch (Exception e) {
            Alert alert = new Alert(null);

            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setTitle("Oops");
            alert.setHeaderText("Bad server =(");
            alert.setContentText("For some reason it was not possible to get the times from the server.\nPlease try again or call Daniel");

// Create expandable Exception.
            //Code from http://code.makery.ch/blog/javafx-dialogs-official/
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);

            e.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("The exception stacktrace was:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

// Set expandable Exception into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();

        }

    }

}
