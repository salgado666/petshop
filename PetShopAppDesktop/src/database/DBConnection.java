package database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    public Connection con;
    public Statement st;
    public ResultSet rs;

    /**
     * This method is responsible to connect to the database
     * The path to database is hard coded
     */
    public DBConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            //con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpetshoptotem", "root", "22253737");
            //con = DriverManager.getConnection("jdbc:mysql://sql12.freemysqlhosting.net:3306/sql12196516", "sql12196516", "qpVaWdRNiD");
            con = DriverManager.getConnection("jdbc:mysql://mysql8.db4free.net:3307/dbpetshopapp", "salgado666", "22253737");
            //https://mysql8.db4free.net/
            st = con.createStatement();

        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Erro: " + e);
        }
    }

    /**
     * Generic method to execute a statement with no return results
     * @author Daniel Salgado
     * @param sql String sql to insert or update the record
     */
    public void save(String sql) {

        try {
            st.execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    /**
     * This method will execute a SQL statement and return result if exist
     * @param sql statement for being executed in database
     * @return ResultSet
     */
    public ResultSet query(String sql) {

        ResultSet res = null;

        try {
            res = st.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

}
