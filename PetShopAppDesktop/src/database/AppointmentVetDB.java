/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import appointment.Appointment;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Salgado
 */
public class AppointmentVetDB {

    Appointment appointmentDB = null;

    /** The constructor will receive the Appointment Class so this can extract data from it
     * @param newAppointment Appoint class
     */
    public AppointmentVetDB(Appointment newAppointment) {
        this.appointmentDB = newAppointment;
    }


    /**
     * This method will generate the insert statement to MySQL and then call the DBConnectionto execute it
     */
    public void save() {

        //dealing with date
        //Date dt = appointmentDB.getDtAppointment();
        //Timestamp timestamp = new java.sql.Timestamp(dt.getTime());
        int linesAffected = 0;

        String sql = ""
                + "INSERT INTO appointment "
                + "(petName "
                + ",petSpecie "
                + ",petBreed "
                + ",petResponsiblePhone "
                + ",petResponsibleEmail "
                + ",petResponsibleName "
                + ",dtAppointment "
                + ",petTypeAppointment "
                + ") VALUES ("
                + "'" + appointmentDB.getPetName() + "'"
                + ",'" + appointmentDB.getPetSpecie() + "'"
                + ",'" + appointmentDB.getPetBreed() + "'"
                + ",'" + appointmentDB.getPetResponsiblePhone() + "'"
                + ",'" + appointmentDB.getPetResponsibleEmail() + "'"
                + ",'" + appointmentDB.getPetResponsibleName() + "'"
                //+ ",'" + appointmentDB.getDtAppointment() + "'"
                + ",'" + new java.sql.Timestamp(appointmentDB.getDtAppointment().getTime()) + "'"
                + ",'" + appointmentDB.getPetTypeAppointment() + "'"
                + ")";

        System.out.println(sql);

        DBConnection con = new DBConnection();

        con.save(sql);

    }

    /**
     * This method will create a query and return a list of strings of available times for that day
     * @return availableTimes
     */
    public List<String> getAvailableTimes() {

        List<String> availableTimes = new ArrayList<String>();
        List<String> timesTaken = new ArrayList<String>();

        availableTimes.add("09:00");
        availableTimes.add("10:00");
        availableTimes.add("11:00");
        availableTimes.add("12:00");
        availableTimes.add("13:00");
        availableTimes.add("14:00");
        availableTimes.add("15:00");
        availableTimes.add("16:00");
        availableTimes.add("17:00");
        availableTimes.add("18:00");
        availableTimes.add("19:00");
        availableTimes.add("20:00");

        String sql = "select Cast(Time_format(dtAppointment,'%H:%i') as char) as t ";
        sql += "from appointment ";
        sql += "where Date(dtAppointment) = '" + appointmentDB.getDtAppointment() + "'";

        ResultSet res = null;

        //int x = 1/0;
        try {

            DBConnection con = new DBConnection();

            res = con.query(sql);

            while (res.next()) {
                timesTaken.add(res.getString("t"));
            }

            for (String item : timesTaken) {
                while (availableTimes.remove(item)) {
                }
            }

        } catch (Exception e) {
            System.out.println("Error trying to get available times from server: " + e.getMessage());
            throw e;
        } finally {
            return availableTimes;
        }

    }

    /**
     * This method will generate the query scripts to return all appointments stored in database
     * @return appointments
     */
    public List<Appointment> getAppointments() {

        String sql = "Select * from appointment";
        List<Appointment> appointments = new ArrayList<Appointment>();

        ResultSet res = null;

        try {

            DBConnection con = new DBConnection();

            res = con.query(sql);

            while (res.next()) {

                Appointment appointment = new Appointment();

                appointment.setIdAppointment(res.getInt("idAppointment"));
                appointment.setPetName(res.getString("petName"));
                appointment.setPetBreed(res.getString("petBreed"));
                appointment.setPetResponsibleName(res.getString("petResponsibleName"));
                appointment.setDtAppointment(res.getTimestamp("dtAppointment"));
                appointment.setPetSpecie(res.getString("petSpecie"));
                appointment.setPetResponsibleEmail(res.getString("petResponsibleEmail"));
                appointment.setPetTypeAppointment(res.getString("petTypeAppointment"));
                appointment.setPetResponsiblePhone(res.getString("PetResponsiblePhone"));

                appointments.add(appointment);

            }
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return appointments;

    }

    /**
     * This method will delete from database an appointment
     * @return true or false
     */
    public boolean cancelAppointment()
    {
        
        String sql = "delete from appointment where idAppointment = " + appointmentDB.getIdAppointment();
        
        System.out.println(sql);

        DBConnection con = new DBConnection();

        con.save(sql);

        return true;

    }
    
}
