/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author danie
 */
public class UserDB {

    public boolean checkUserPwd(String login, String password) throws SQLException {
        
        boolean isValid = false;
        
        String sql = "select Login, Password from user where ";
        sql += " Login = '" + login + "'";
        sql +=  " and Password = '" + password + "'";
        sql += " limit 1";
        
        ResultSet res = null;
        
        DBConnection con =  new DBConnection();
        
        res = con.query(sql);
        
        try 
        {
            res.first();
            isValid = (login.equals(res.getString("Login")) && password.equals(res.getString("Password")));
        } catch (SQLException ex) {
            Logger.getLogger(UserDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        
        return isValid;
        
    }

}
