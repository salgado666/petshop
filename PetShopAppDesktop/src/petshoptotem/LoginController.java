/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petshoptotem;

import admin.AdminUser;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController implements Initializable {

    @FXML
    private TextField txtUserName;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Label lblLoginResult;

    @FXML
    private void handleLoginButtonAction(ActionEvent event) throws Exception {

        System.out.println("petshoptotem.LoginController.handleLoginButtonAction()");
        String sceneToLoad;

        if (((Button) event.getSource()).getId().equals("btnAdmin")) {
            sceneToLoad = "/appointment/AppointmentList.fxml";
        } else {
            sceneToLoad = "PetShopTotemMain.fxml";
        }

        boolean isValid = false;
        AdminUser userLogin = new AdminUser();

        userLogin.setLogin(txtUserName.getText());
        userLogin.setPassword(txtPassword.getText());

        if (txtUserName.getText().equals("") && txtPassword.getText().equals("")) {
            lblLoginResult.setText("Invalid Login");

          
        } else {

            isValid = userLogin.checkUserPassword();

            if (isValid == true) {

                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource(sceneToLoad));

                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();

            } else {

                txtUserName.setText("");
                txtPassword.setText("");
                txtUserName.setFocusTraversable(true);

                lblLoginResult.setText("Invalid Login");

            }

        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
