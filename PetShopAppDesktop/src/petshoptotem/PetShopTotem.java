/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petshoptotem;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author danie
 */
public class PetShopTotem extends Application {



    @Override
    public void start(Stage stage) throws Exception {

        String firstScreen = "Login.fxml";

        Parent root = FXMLLoader.load(getClass().getResource(firstScreen));

        Scene scene = new Scene(root);

        stage.setTitle("Welcome");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setMaximized(false);
        stage.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

  
}
